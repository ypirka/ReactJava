package processor;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Flow;

public class ConverterToPNG implements Flow.Processor<File, File> {
    private Flow.Subscription subscription;
    @Override
    public void subscribe(Flow.Subscriber<? super File> subscriber) {

    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
    this.subscription = subscription;
    subscription.request(1);

    }

    @Override
    public void onNext(File item) {
     subscription.request(1);
     imageTransCoding(item);
    }

    @Override
    public void onError(Throwable throwable) {
      throwable.printStackTrace();
    }

    @Override
    public void onComplete() {
     subscription.cancel();
    }
    public File imageTransCoding(File file){

        File pngFile = new File(file.getName()+".png");
        try {
            ImageIO.write((RenderedImage) file,"png",pngFile);
        } catch (IOException e) {

        }
        return pngFile;
    }

}
